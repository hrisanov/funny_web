package main

import (
	"log"
	"net/http"
)

func main() {
	// Определяем каталог, в котором хранятся ваши файлы HTML
	staticDir := "./shop/"

	// Создаем обработчик файлового сервера, который обслуживает файлы из указанного каталога
	fileServer := http.FileServer(http.Dir(staticDir))

	// Регистрируем файловый сервер для обработки запросов на статические файлы
	http.Handle("/", fileServer)

	// Запускаем веб-сервер на порту 8080
	log.Println("Server started on http://127.0.0.1:8081")
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal(err)
	}
}


